import { task, watch, src, dest, series } from 'gulp';
import sass from 'gulp-sass';

task('sass', () => {
  return src('src/scss/main.scss')
    .pipe(sass()) // Converts Sass to CSS with gulp-sass
    .pipe(dest('public/css'));
});

task('watch', () => {
  watch('src/scss/**/*.scss', series(['sass']));
});
