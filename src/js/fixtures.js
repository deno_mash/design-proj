let user = {
  name: 'Jessica Parker',
  location: 'Newport Beach, CA',
  phone: '(949) 325 - 68594',
  reviews: 6,
  followers: 15,
  website: 'www.seller.com'
};

export default user;
