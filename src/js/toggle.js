const toggle = () => {
  let width;

  const aboutBtn = document.getElementById('about');
  const settingsBtn = document.getElementById('settings');

  const aboutSectionClass = document.getElementsByClassName('about-section')[0];
  const aboutSectionMobile = document.getElementsByClassName(
    'about-section__mobile'
  )[0];
  const settingsSectionClass = document.getElementsByClassName(
    'settings-section'
  )[0];

  aboutBtn.onclick = () => {
    width = window.innerWidth > 0 ? window.innerWidth : screen.width;

    aboutSectionClass.style.display = 'block';
    settingsSectionClass.style.display = 'none';

    if (width <= 480 && width >= 320) {
      aboutSectionMobile.style.display = 'block';
    } else {
      aboutSectionMobile.style.display = 'none';
    }
  };

  settingsBtn.onclick = () => {
    aboutSectionClass.style.display = 'none';
    settingsSectionClass.style.display = 'flex';
    aboutSectionMobile.style.display = 'none';
  };
};

export default toggle;
