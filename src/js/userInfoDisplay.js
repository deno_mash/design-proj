import user from './fixtures';

const { name, location, reviews, phone, followers, website } = user;

const userInfoDispay = () => {
  const editMobile = document.getElementsByClassName(
    'about-section__edit--mobile'
  )[0];
  const updateInfoMobile = document.getElementsByClassName(
    'about-section__save-cancel'
  )[0];

  const cancelMobile = document.getElementById('cancel-mobile');
  const saveMobile = document.getElementById('save-mobile');

  document.getElementById('user-name').innerHTML = name;
  document.getElementById('location').innerHTML = location;
  document.getElementById('reviews').innerHTML = reviews;
  document.getElementById('phone').innerHTML = phone;
  document.getElementById('followers').innerHTML = followers;

  //   document.getElementById('close').onclick = () => {
  //     document.getElementById('tool-tip').style.display = 'none';
  //   };

  editMobile.onclick = () => {
    editMobile.style.display = 'none';
    updateInfoMobile.style.display = 'flex';
  };

  cancelMobile.onclick = () => {
    editMobile.style.display = 'flex';
    updateInfoMobile.style.display = 'none';
  };

  saveMobile.onclick = () => {
    editMobile.style.display = 'flex';
    updateInfoMobile.style.display = 'none';
  };

  const content = `
        <div class="about-section__info">
            <div class="u-margin-left-very-small">${name}</div>
            <div class="about-section__edit" onclick="edit()">
                <i class="fas fa-pencil-alt fa-lg"></i>
            </div>
        </div>

        <div class="about-section__info">
        <div class="u-margin-left-very-small">
            <i class="light-grey fas fa-globe-americas"></i>
            <span class="u-margin-left-very-small">${website}</span>
        </div>
        <div class="about-section__edit">
            <i class="fas fa-pencil-alt fa-lg"></i>
        </div>
        </div>

        <div class="about-section__info">
        <div class="u-margin-left-very-small">
            <i class="light-grey fas fa-phone-alt"></i>
            <span class="u-margin-left-very-small">${phone}</span>
        </div>
        <div class="about-section__edit">
            <i class="fas fa-pencil-alt fa-lg"></i>
        </div>
        </div>

        <div class="about-section__info">
        <div class="u-margin-left-very-small">
            <i class="light-grey fas fa-home"></i>
            <span class="u-margin-left-very-small">${location}</span>
        </div>
        <div class="about-section__edit">
            <i class="fas fa-pencil-alt fa-lg"></i>
        </div>
        </div>
`;

  const info = `
        <div class="about-section__info">
                ${name}
            </div>

            <div class="about-section__info">
                <div class="u-margin-left-very-small">
                <i class="light-grey fas fa-globe-americas"></i>
                <span class="u-margin-left-very-small">${website}</span>
                </div>
            </div>

            <div class="about-section__info">
                <div class="u-margin-left-very-small">
                <i class="light-grey fas fa-phone-alt"></i>
                <span class="u-margin-left-very-small">${phone}</span>
                </div>
            </div>

            <div class="about-section__info">
                <div class="u-margin-left-very-small">
                <i class="light-grey fas fa-home"></i>
                <span class="u-margin-left-very-small">${location}A</span>
                </div>
            </div>`;

  const editTooltip = `
            <!-- Tooltip -->
          <div class="tooltip">
            <div class="tooltip__text">
              <div class="tooltip__title">City, state & zip</div>

              <input class="input__field" type="text" value="John Doe" />
              <div class="tooltip__btns">
                <div class="btn btn--solid-blue">save</div>
                <div class="btn btn--outline-blue">cancel</div>
              </div>
            </div>
          </div>
          <!-- Tooltip End -->
          `;

  document.getElementById('about-content').innerHTML = content;
  document.getElementsByClassName('about-section__mobile')[0].innerHTML = info;
};

export default userInfoDispay;
